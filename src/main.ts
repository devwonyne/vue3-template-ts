import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import vueConfig, {
  store,
  VueLogger,
  VueLoggerOptions,
} from "./config/vue-config";

const _app = createApp(App);

_app.use(VueLogger, VueLoggerOptions);
_app.use(store);
_app.use(vueConfig);
_app.use(router);
_app.mount("#app");
