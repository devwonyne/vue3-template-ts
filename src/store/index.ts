import Vuex from "vuex";
import { CommonStore } from "./biz/CommonStore";

export interface IRootStore {
  commonStore: CommonStore;
}

export default new Vuex.Store<IRootStore>({});
