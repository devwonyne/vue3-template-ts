import {
  VuexModule,
  Mutation,
  Action,
  getModule,
  Module,
} from "vuex-module-decorators";
import store from "..";

export interface CommonStore {
  /**
   *
   */
  isOnLoading: boolean;
}

@Module({ dynamic: true, namespaced: true, name: "commonStore", store })
class CommonStoreImpl extends VuexModule implements CommonStore {
  isOnLoading = false;

  @Mutation
  setIsOnLoading(flag: boolean) {
    this.isOnLoading = flag;
  }

  @Action
  updateIsOnLoading(flag: boolean): void {
    this.setIsOnLoading(flag);
  }
}
export default getModule(CommonStoreImpl);
