/** dashboard css (tailwind) */
import "@/assets/css/tailwind.css";

import Api from "@/helper/api";
import { ApiHelper } from "@/helper/ApiHelper";
import CommonStoreImpl, { CommonStore } from "@/store/biz/CommonStore";
import store from "@/store";
import { VueLogger, VueLoggerOptions } from "./logger-confing";

/**
 * all components
 */
const cmps = require.context("@/components", true, /\.(vue)$/i);

const registerComponents = (app: any, cmps: any): any => {
  cmps.keys().map((key: string) => {
    const match = key.match(/\w+(\w+.vue)/);
    if (match) {
      const cmpNm = match[0].replaceAll(".vue", "");
      return app.component(cmpNm, cmps(key).default);
    }
  });
};

/**
 * global custom properties's interface !!
 */
declare module "@vue/runtime-core" {
  export interface ComponentCustomProperties {
    $api: ApiHelper;

    $commonStore: CommonStore;
  }
}

export { store, VueLogger, VueLoggerOptions };

export default {
  install: (app: any) => {
    let installed = false;
    if (installed) {
      return;
    }
    installed = true;
    /**
     * initialize components
     */
    registerComponents(app, cmps);
    /**
     * global custom properties !!
     */
    app.config.globalProperties.$api = Api.v1;
    app.config.globalProperties.$commonStore = CommonStoreImpl;
  },
};
