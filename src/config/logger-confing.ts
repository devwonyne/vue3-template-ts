import VueLogger from "vuejs3-logger";

const isProduction = process.env.NODE_ENV === "production";

const VueLoggerOptions = {
  isEnabled: true,
  logLevel: isProduction ? "error" : "debug",
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: true,
  separator: "|",
  showConsoleColors: true,
};

export { VueLogger, VueLoggerOptions };
