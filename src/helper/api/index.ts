import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import ApiHelperImpl, { ApiHelper } from "../ApiHelper";
export class Api {
  /** Axios*/
  nAxios: AxiosInstance;
  v1: ApiHelper;

  constructor() {
    this.nAxios = axios.create({
      headers: {
        common: {
          "Content-Type": "application/json; charset=utf-8",
          withCredentials: true,
        },
      },
    });
    this.nAxios.defaults.withCredentials = true;
    //this.nAxios.defaults.baseURL = "";
    this.nAxios.interceptors.request.use(
      this.requestInterceptor,
      this.errorInterceptor
    );

    // --
    this.v1 = new ApiHelperImpl(this.nAxios);
  }

  /**
   * Interceptor for request
   * @param request
   */
  requestInterceptor = async (request: AxiosRequestConfig) => {
    return request;
  };

  /**
   * Interceptor for response
   * @param response
   */
  errorInterceptor = async (response: any) => {
    if (response.state !== "200") {
      alert(`[오류: ${response.data.code}] ${response.data.message}`);
    }
    return response;
  };
}

export default new Api();
