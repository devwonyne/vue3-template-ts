import { AxiosInstance } from "axios";

/**
 * interface
 */
export interface ApiHelper {
  login(): Promise<any>;
}

/**
 * Class
 */
export default class ApiHelperImpl implements ApiHelper {
  #nApi: AxiosInstance;
  constructor(axios: AxiosInstance) {
    this.#nApi = axios;
  }
  login(): Promise<any> {
    throw new Error("Method not implemented.");
  }
}
